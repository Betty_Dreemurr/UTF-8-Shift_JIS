# UTF-8 to Shift_JIS Converter

[![License](https://img.shields.io/badge/license-GPL--3.0-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## 特徴

- UTF-8からShift_JISへの簡単な変換。
- 直感的なコマンドラインインターフェイス
- 結果がカラーで表示され、読みやすい
- プログラムを終了したり、メインメニューに戻ったり、別の変換を実行したりするオプション。

## 使用方法

1. システムにPython 3がインストールされていることを確認する(+ pythonモジュール[colorama](https://pypi.org/project/colorama/))
2. GitHub リポジトリをクローンするか、`[language].py` ファイルをダウンロードしてください。
3. コマンドプロンプト/ターミナルを開き、`[language].py`ファイルのあるディレクトリに移動します。
4. プログラムを実行し、メニューの指示に従って必要な変換を行う。
5. 貢献は大歓迎です！このプログラムを改良したい場合は、遠慮なく Pull Request を作成してください。

## ライセンス

このプログラムはGPL-3.0でライセンスされています。詳細については、[LICENSE](LICENSE)ファイルを参照してください。
